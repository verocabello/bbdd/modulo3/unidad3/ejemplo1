﻿-- Modulo 3 Unidad 3

-- EJEMPLO 1

-- 1
-- procedimiento almacenado que reciba dos numeros y te indique el mayor, con instruccion if

DELIMITER //
CREATE OR REPLACE PROCEDURE ej1v1(a int, b int)
  BEGIN
    DECLARE resultado int;
    IF a>=b THEN
      SET resultado=a;
    ELSE
      SET resultado=b;
      END IF;
      SELECT resultado;
  END //
DELIMITER;

  CALL ej1v1(12,23);

-- recibir dos numeros y te indique el mayor, con consulta de totales

DELIMITER //
CREATE OR REPLACE PROCEDURE ej1v2(a int,b int)
  BEGIN
    CREATE TEMPORARY TABLE tmp(
      num int
    );
    INSERT INTO tmp (num)
      VALUES (a),(b);
    SELECT MAX(num) FROM tmp;
  END //
DELIMITER;

CALL ej1v2(7,8);


-- procedimiento almacenado que reciba dos numeros y te indique el mayor, con funcion de mysql

CREATE OR REPLACE FUNCTION ej1v3(a int, b int)
  RETURNS int
  BEGIN
    DECLARE resultado int;
    IF a>=b THEN
      SET resultado=a;
    ELSE
      SET resultado=b;
      END IF;
      RETURN resultado;
  END;

SELECT ej1v3(12,23);

USE provincias;

-- 2
-- procedimiento almacenado que reciba dos numeros y te indique el mayor, con instruccion if

DELIMITER //
CREATE OR REPLACE PROCEDURE ej2v1(a int, b int, c int)
  BEGIN
    DECLARE resultado int;
    IF a>b THEN
      IF a>c THEN
        SET resultado=a;
      ELSE
        SET resultado=c;
      END IF;
    ELSE
      IF b>c THEN
        SET resultado=b;
      ELSE
        SET resultado=c;
      END IF;
    END IF;
      SELECT resultado;
  END //
DELIMITER;
  
  CALL ej2v1(12,58,55);

-- recibir dos numeros y te indique el mayor, con consulta de totales

DELIMITER //
CREATE OR REPLACE PROCEDURE ej2v2(a int,b int,c int)
  BEGIN
  DROP TABLE IF EXISTS tmp;
    CREATE TEMPORARY TABLE tmp(
      num int
    );
    INSERT INTO tmp (num)
      VALUES (a),(b),(c);
    SELECT MAX(num) FROM tmp;
  END //
DELIMITER;

CALL ej2v2(12,23,55);

-- procedimiento almacenado que reciba dos numeros y te indique el mayor, con funcion de mysql

CREATE OR REPLACE FUNCTION ej2v3(a int, b int, c int)
  RETURNS int
  BEGIN
    DECLARE resultado int;
    IF a>b THEN
      IF a>c THEN
        SET resultado=a;
      ELSE
        SET resultado=c;
      END IF;
    ELSE
      IF b>c THEN
        SET resultado=b;
      ELSE
        SET resultado=c;
      END IF;
    END IF;
    RETURN resultado;
  END;

SELECT ej2v3(12,23,55);

-- 3
-- procedimiento almacenado que reciba tres numeros y dos argumentos de tipo salida donde devuelva el numero mas grande y el numero mas pequeño de los tres numeros pasados

DELIMITER //
CREATE OR REPLACE PROCEDURE ej3v1(a int, b int, c int, OUT v1 int, OUT v2 int)
  BEGIN
   set v1=(SELECT GREATEST(a,b,c));
   set v2=(SELECT LEAST(a,b,c));
  END //
DELIMITER;

CALL ej3v1(45,22,67,@a,@b);
SELECT @a,@b;


-- 4
-- procedimiento almacenado que reciba dos fechas y te muestre el numero de dias de diferencia entre las dos fechas

DELIMITER //
CREATE OR REPLACE PROCEDURE ej4(fecha1 date,fecha2 date)
  BEGIN
    SELECT DATEDIFF(fecha1,fecha2);
  END //
DELIMITER;

CALL ej4('2019-08-05','2019-10-07');
SELECT DATEDIFF('2019-08-05','2019-10-07');

-- 5
-- procedimiento almacenado que reciba dos fechas y te muestre el numero de diferencia entre las dos fechas

DELIMITER //
CREATE OR REPLACE PROCEDURE ej5(fecha1 date,fecha2 date)
  BEGIN
    SELECT TIMESTAMPDIFF(month,fecha1,fecha2);
  END //
DELIMITER;

CALL ej5('2019-08-05','2019-10-07');
SELECT TIMESTAMPDIFF(month,'2019-08-05','2019-10-07');

-- 6
-- procedimiento almacenado que reciba dos fechas y te devuelva en 3 algumentos de salida los dias, meses y años entre las dos fechas

DELIMITER //
CREATE OR REPLACE PROCEDURE ej6(fecha1 date, fecha2 date, OUT dias int, OUT meses int, OUT annos int)
  BEGIN
    SET dias=TIMESTAMPDIFF(DAY,fecha1,fecha2);
    SET meses=TIMESTAMPDIFF(MONTH,fecha1,fecha2);
    SET annos=TIMESTAMPDIFF(YEAR,fecha1,fecha2);
  END //
DELIMITER;

CALL ej6('2018-08-05','2005-06-12',@dias,@meses,@annos);
SELECT @dias,@meses,@annos;

-- 7
-- procedimiento almacenado que reciba una frase y te muestre el numero de caracteres

DELIMITER //
CREATE OR REPLACE PROCEDURE ej7(frase varchar(100))
  BEGIN
    SELECT LENGTH(frase);
  END //
DELIMITER;

CALL ej7('numero de caracteres de una frase');

